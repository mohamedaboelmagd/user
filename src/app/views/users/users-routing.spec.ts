import { UsersComponent } from './containers';
import { ROUTES } from './users-routing.module';

describe('users-routes', () => {
  it('should contain a route for component', () => {
    expect(ROUTES).toContain({
      path: '',
      component: UsersComponent
    });
  });
});
