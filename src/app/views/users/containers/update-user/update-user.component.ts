import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IAlert } from '@models/alert.models';
import { Config } from 'protractor';

import { UserService } from './../../../login/services';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {
  @Input()
  user;
  @Output()
  dismissModel = new EventEmitter();

  @Output()
  message = new EventEmitter<IAlert>();
  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {}
  form: FormGroup;
  ngOnInit() {
    this.form = this.formBuilder.group({
      name: this.user ? this.user.first_name : '',
      jobTitle: ''
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const { name, jobTitle } = this.form.value;
      this.userService
        .update(this.user.id, { name, jobTitle })
        .then((data: Config) => {
          this.message.emit({
            type: 'success',
            message: 'user is updated successfully'
          });
          this.closeModel();
        })
        .catch(error => {
          this.message.emit({
            type: 'warn',
            message: 'user is not updated successfully'
          });
        });
    }
  }

  closeModel() {
    this.dismissModel.emit(true);
  }
}
