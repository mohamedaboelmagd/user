import { UsersComponent } from './users/users.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';

export const CONTAINERS = [
  UsersComponent,
  CreateUserComponent,
  UpdateUserComponent,
  DeleteUserComponent
];

export * from './users/users.component';
export * from './create-user/create-user.component';
export * from './update-user/update-user.component';
export * from './delete-user/delete-user.component';
