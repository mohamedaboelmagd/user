import { Component, OnInit } from '@angular/core';
import { IAlert } from '@models/alert.models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';

import { UserService } from './../../../login/services';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users$: Observable<any> = null;

  hide = true;

  editableUser;

  alert: IAlert = null;

  constructor(
    private userService: UserService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.users$ = this.userService.getAll();
  }

  showAlert($event) {
    this.alert = $event;
    setTimeout(() => (this.alert = null), 5000);
  }

  close() {
    this.alert = null;
  }

  hidePanel($event) {
    this.hide = $event;
  }

  setEditableUser($event) {
    this.editableUser = $event;
  }

  closeModel($event) {
    this.modalService.dismissAll();
  }
}
