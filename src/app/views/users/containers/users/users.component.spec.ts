import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NgbAlertModule,
  NgbCollapseModule,
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';

import { UserService } from '../../../login/services';
import * as fromComponents from './../../components';
import {
  CreateUserComponent,
  DeleteUserComponent,
  UpdateUserComponent
} from './../../containers';
import { UsersComponent } from './users.component';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgbAlertModule,
        NgbCollapseModule
      ],
      declarations: [
        UsersComponent,
        ...fromComponents.COMPONENTS,
        CreateUserComponent,
        UpdateUserComponent,
        DeleteUserComponent
      ],
      providers: [UserService, NgbModal]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load users from the server', () => {
    const service = TestBed.get(UserService);
    spyOn(service, 'getAll').and.returnValue(() => of({ data: [1, 2, 3] }));
    component.ngOnInit();
    expect(component.users$).not.toEqual(null);
  });
});
