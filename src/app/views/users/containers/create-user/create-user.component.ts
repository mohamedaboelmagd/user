import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IAlert } from '@models/alert.models';
import { Config } from 'protractor';

import { UserService } from './../../../login/services';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  form: FormGroup;

  @Output()
  dismissModel = new EventEmitter();

  @Output()
  message = new EventEmitter<IAlert>();
  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {}
  ngOnInit() {
    this.form = this.formBuilder.group({
      name: '',
      jobTitle: ''
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const { name, jobTitle } = this.form.value;
      this.userService
        .create({ name, jobTitle })
        .then((data: Config) => {
          this.message.emit({
            type: 'success',
            message: 'user is created successfully'
          });
          this.closeModel();
        })
        .catch(error => {
          this.message.emit({
            type: 'warn',
            message: 'user is not created successfully'
          });
        });
    }
  }

  closeModel() {
    this.dismissModel.emit(true);
  }
}
