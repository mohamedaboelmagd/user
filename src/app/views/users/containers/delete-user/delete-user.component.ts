import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IAlert } from '@models/alert.models';
import { Config } from 'protractor';

import { UserService } from './../../../login/services/user.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {
  @Input()
  user;
  @Output()
  dismissModel = new EventEmitter();

  @Output()
  message = new EventEmitter<IAlert>();
  constructor(private userService: UserService) {}
  ngOnInit() {}

  delete() {
    this.userService
      .delete(this.user.id)
      .then((data: Config) => {
        this.message.emit({
          type: 'success',
          message: 'user is deleted successfully'
        });
        this.closeModel();
      })
      .catch(error => {
        this.message.emit({
          type: 'warn',
          message: 'user is not deleted successfully'
        });
      });
  }

  closeModel() {
    this.dismissModel.emit(true);
  }
}
