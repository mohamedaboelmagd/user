import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NgbAlertModule,
  NgbCollapseModule,
  NgbModalModule
} from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../login/services';
import * as fromComponents from './components';
import * as fromContainers from './containers';
import * as fromPipes from './pipes';
import { UsersRoutingModule } from './users-routing.module';

export const BOOTSTRAP = [NgbCollapseModule, NgbModalModule, NgbAlertModule];
@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    ...BOOTSTRAP,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    ...fromContainers.CONTAINERS,
    ...fromComponents.COMPONENTS,
    ...fromPipes.PIPES
  ],
  providers: [UserService]
})
export class UsersModule {}
