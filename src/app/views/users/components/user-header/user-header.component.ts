import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-header',
  templateUrl: './user-header.component.html',
  styleUrls: ['./user-header.component.scss']
})
export class UserHeaderComponent {
  @Input()
  modal: string;
  constructor(private modalService: NgbModal) {}

  openCreateUser(content) {
    this.modalService.open(content, {
      size: 'sm',
      centered: true
    });
  }
}
