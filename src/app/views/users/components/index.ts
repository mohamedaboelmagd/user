import { UserHeaderComponent } from './user-header/user-header.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UserViewComponent } from './user-view/user-view.component';

export const COMPONENTS = [
  UserHeaderComponent,
  UsersListComponent,
  UserViewComponent
];
