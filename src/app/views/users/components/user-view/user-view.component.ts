import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent {
  @Input()
  hide = true;

  @Output()
  selectedUser = new EventEmitter();

  @Input()
  modalEdit: string;
  @Input()
  modalDelete: string;

  @Input()
  user;

  @Output()
  hideEmitter = new EventEmitter();

  constructor(private modalService: NgbModal) {}

  openEditUser(content, user) {
    this.modalService.open(content, {
      size: 'sm',
      centered: true
    });
    this.selectedUser.emit(user);
  }

  hidePanel() {
    this.hideEmitter.emit(true);
  }
}
