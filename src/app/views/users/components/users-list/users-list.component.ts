import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IAlert } from '@models/alert.models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../../../login/services';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent {
  @Input()
  users;

  @Input()
  modalEdit: string;
  @Input()
  modalDelete: string;

  @Output()
  selectedUser = new EventEmitter();

  @Output()
  hidePanel = new EventEmitter();

  @Output()
  message = new EventEmitter<IAlert>();
  constructor(
    private userService: UserService,
    private modalService: NgbModal
  ) {}

  getUser(id) {
    this.hidePanel.emit(false);
    this.userService
      .getOne(id)
      .then(result => {
        this.message.emit({
          type: 'success',
          message: 'user is loaded successfully'
        });
        this.selectedUser.emit(result.data);
      })
      .catch(err => {
        this.message.emit({
          type: 'warn',
          message: 'user is not loaded successfully'
        });
      });
  }

  openEditUser(content, user) {
    this.modalService.open(content, {
      size: 'sm',
      centered: true
    });
    this.selectedUser.emit(user);
  }
}
