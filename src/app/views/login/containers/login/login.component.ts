import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IAlert } from '@models/alert.models';
import { Config } from 'protractor';

import { UserService } from '../../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {}

  form: FormGroup;

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  alert: IAlert;

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: '',
      password: ''
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const { email, password } = this.form.value;
      this.userService.login({ email, password }).subscribe(
        (data: Config) => {
          this.alert = {
            type: 'success',
            message: 'login works successfully'
          };
          this.router.navigate(['/users']);
        },
        error => (this.alert = { type: 'danger', message: error.error.error })
      );
    }
  }
}
