import { UsersComponent } from './../../../users/containers/users/users.component';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of, empty, Observable } from 'rxjs';

import { UserService } from './../../services/user.service';
import { LoginComponent } from './login.component';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  let userService: UserService;

  beforeEach(async(() => {
    const mockRouter = { navigate: jasmine.createSpy('navigate') };
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([
          {
            path: 'users',
            component: UsersComponent
          }
        ])
      ],
      declarations: [LoginComponent, UsersComponent],
      providers: [UserService],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    userService = TestBed.get(UserService);

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create a form with 2 controls', () => {
    expect(component.form.contains('email')).toBeTruthy();
    expect(component.form.contains('password')).toBeTruthy();
  });

  it('should userService throw error', () => {
    const error = 'Missing password';
    const spy = spyOn(userService, 'login').and.callFake(() =>
      Observable.throw({
        error: error
      })
    );

    expect(spy).toThrowError();
  });

  it('should clicks on button submit', () => {
    const button = fixture.debugElement.query(By.css('.submit'));
    button.triggerEventHandler('click', null);

    expect(component.form.valid).toBeTruthy();
  });

  it('should login navigate to users', () => {
    const router = TestBed.get(Router);
    const spy = spyOn(router, 'navigate');

    spyOn(userService, 'login').and.callFake(() => of({ data: 'any' }));
    component.onSubmit();

    expect(spy).toHaveBeenCalledWith(['/users']);
  });

  it('should show error message in html', () => {
    const error = 'Missing password';
    component.alert = { type: 'danger', message: error };
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('.alert'));
    const el: HTMLElement = de.nativeElement;

    expect(el.innerText).toContain(error);
  });
});
