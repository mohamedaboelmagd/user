import { LoginComponent } from './containers';
import { ROUTES } from './login-routing.module';

describe('login-routes', () => {
  it('should contain a route for component', () => {
    expect(ROUTES).toContain({
      path: '',
      component: LoginComponent
    });
  });
});
