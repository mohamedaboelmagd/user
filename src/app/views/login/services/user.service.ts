import { Injectable } from '@angular/core';
import { API } from '@config/user.config';

import { ApiService } from '../../../services';
@Injectable()
export class UserService extends ApiService {
  path = `${API.base}${API.users}`;

  login(data: { email: string; password: string }) {
    return this.http.post(`${API.base}${API.login}`, data);
  }
}
