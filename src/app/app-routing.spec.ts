import { ROUTES } from './app-routing.module';

describe('app-routes', () => {
  it('should contain a route for /users', () => {
    expect(ROUTES).toContain({
      path: 'users',
      loadChildren: './views/users/users.module#UsersModule'
    });
  });

  it('should contain a route for /', () => {
    expect(ROUTES).toContain({
      path: '',
      loadChildren: './views/login/login.module#LoginModule'
    });
  });
});
