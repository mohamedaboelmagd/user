export const API = {
  base: 'https://reqres.in',
  login: '/api/login',
  users: '/api/users'
};
