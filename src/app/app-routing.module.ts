import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: 'users',
    loadChildren: './views/users/users.module#UsersModule'
  },
  {
    path: '',
    loadChildren: './views/login/login.module#LoginModule'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
