import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public path: string;

  constructor(protected http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get(this.path);
  }

  getOne(id): Promise<any> {
    return this.http.get(`${this.path}/${id}`).toPromise();
  }

  create(data: any): Promise<any> {
    return this.http.post(this.path, data).toPromise();
  }

  update(id, data: any): Promise<any> {
    return this.http.put(`${this.path}/${id}`, data).toPromise();
  }

  delete(id): Promise<any> {
    return this.http.delete(`${this.path}/${id}`).toPromise();
  }
}
